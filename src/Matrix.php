<?php

namespace AHP;

use AHP\Matrix\Column;
use AHP\Matrix\ColumnInterface;
use AHP\Matrix\Row;
use AHP\Matrix\RowInterface;

/**
 * Class Matrix
 * Class wrapping matrix to be used in AHP - inverse symmetric positive matrix
 * @package AHP
 */
class Matrix implements MatrixInterface
{
    /**
     * @var array
     */
    private array $rows = [];
    /**
     * @var array
     */
    private array $columns = [];

    /**
     * Matrix constructor.
     * @param array|null $rows
     * @throws MatrixException
     */
    public function __construct(array $rows = null)
    {
        if (!empty($rows)) {
            $this->setRows($rows);
        }
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return count($this->rows);
    }

    /**
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     * @param array $rows
     * @param bool $createColumnsFromRows
     * @return $this
     * @throws MatrixException
     */
    public function setRows(array $rows, bool $createColumnsFromRows = true): self
    {
        $this->rows = [];
        foreach ($rows as $row) {
            if (!$row instanceof RowInterface) {
                throw new MatrixException(
                    sprintf('Param $rows passed must have values as instances of %s interface.',
                        RowInterface::class)
                );
            }
            $this->rows[$row->getIndex()] = $row;
        }
        if ($createColumnsFromRows) {
            $this->createColumnsFromRows();
        }
        return $this;
    }

    /**
     * @param array $columns
     * @param bool $createRowsFromColumns
     * @return $this
     * @throws MatrixException
     */
    public function setColumns(array $columns, bool $createRowsFromColumns = true): self
    {
        $this->columns = [];
        foreach ($columns as $column) {
            if (!$column instanceof RowInterface) {
                throw new MatrixException(
                    sprintf('Param $columns passed must have values as instances of %s interface.',
                        ColumnInterface::class)
                );
            }
            $this->columns[$column->getIndex()] = $column;
        }
        if ($createRowsFromColumns) {
            $this->createRowsFromColumns();
        }
        return $this;
    }

    /**
     * @param string $rowIndex
     * @return Row
     * @throws MatrixException
     */
    public function getRow(string $rowIndex): RowInterface
    {
        if (empty($this->rows[$rowIndex])) {
            throw new MatrixException(
                sprintf('Matrix has no row with row index %s .', $rowIndex)
            );
        }
        return $this->rows[$rowIndex];
    }

    /**
     * @param RowInterface $row
     * @param bool $override
     * @return $this
     * @throws MatrixException
     */
    public function addRow(RowInterface $row, bool $override = true): self
    {
        $rowIndex = $row->getIndex();
        if (!empty($this->rows[$rowIndex]) && !$override) {
            throw new MatrixException(
                sprintf('Row with row index %s already exists.', $rowIndex)
            );
        }
        $this->rows[$rowIndex] = $row;
        return $this;
    }

    /**
     * @param ColumnInterface $column
     * @param bool $override
     * @return $this
     * @throws MatrixException
     */
    public function addColumn(ColumnInterface $column, bool $override = true): self
    {
        $columnIndex = $column->getIndex();
        if (!empty($this->columns[$columnIndex]) && !$override) {
            throw new MatrixException(
                sprintf('Column with column index %s already exists.', $columnIndex)
            );
        }
        $this->columns[$columnIndex] = $column;
        return $this;
    }

    /**
     * @param string $rowIndex
     * @param string $columnIndex
     * @return float
     * @throws MatrixException
     * @throws Matrix\RowException
     */
    public function getElement(string $rowIndex, string $columnIndex): float
    {
        $row = $this->getRow($rowIndex);
        return $row->getElement($columnIndex);
    }

    /**
     * @param string $rowIndex
     * @param string $columnIndex
     * @param float $value
     * @param bool $setInverseElement
     * @return $this
     * @throws MatrixException
     */
    public function setElement(string $rowIndex, string $columnIndex, float $value,
                               bool $setInverseElement = true): self
    {
        if ($value <= 0) {
            throw new MatrixException(
                sprintf('Element value can not be less or equal zero. Value is %s.', $value)
            );
        }

        if (empty($this->rows[$rowIndex])) {
            $this->rows[$rowIndex] = new Row($rowIndex, []);
        }
        $this->rows[$rowIndex]->setElement($columnIndex, $value);

        if (empty($this->columns[$columnIndex])) {
            $this->columns[$columnIndex] = new Column($columnIndex, []);
        }
        $this->columns[$columnIndex]->setElement($rowIndex, $value);

        return $this;
    }

    /**
     * @param string $rowIndex
     * @param string $columnIndex
     * @return float
     * @throws MatrixException
     * @throws Matrix\RowException
     */
    public function getInverseElement(string $rowIndex, string $columnIndex): float
    {
        return $this->getElement($columnIndex, $rowIndex);
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        if (empty($this->columns)) {
            $this->createColumnsFromRows();
        }
        return $this->columns;
    }

    /**
     * @param string $columnIndex
     * @return ColumnInterface
     * @throws MatrixException
     */
    public function getColumn(string $columnIndex): ColumnInterface
    {
        if (empty($this->columns)) {
            $this->createColumnsFromRows();
        }
        if (empty($this->columns[$columnIndex])) {
            throw new MatrixException(
                sprintf('Matrix has no column with row index %d .', $columnIndex)
            );
        }
        return $this->columns[$columnIndex];
    }

    /**
     * Creates columns based on rows in matrix
     */
    public function createColumnsFromRows(): void
    {
        /** @var Row $row */
        foreach ($this->rows as $rowIndex => $row) {
            foreach ($row->getElements() as $columnIndex => $number) {
                if (empty($this->columns[$columnIndex])) {
                    $this->columns[$columnIndex] = new Column($columnIndex, []);
                }
                $this->columns[$columnIndex]->setElement($rowIndex, $number);
            }
        }
    }

    /**
     * Creates rows based on columns in matrix
     */
    public function createRowsFromColumns(): void
    {
        /** @var ColumnInterface $column */
        foreach ($this->columns as $columnIndex => $column) {
            foreach ($column->getElements() as $rowIndex => $number) {
                if (empty($this->rows[$rowIndex])) {
                    $this->rows[$rowIndex] = new Row($rowIndex, []);
                }
                $this->rows[$rowIndex]->setElement($columnIndex, $number);
            }
        }
    }
}