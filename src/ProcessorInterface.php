<?php

namespace AHP;

/**
 * Interface NodeInterface
 * @package AHP
 */
interface ProcessorInterface
{
    /**
     * @param NodeInterface $taskNode
     * @return string
     * @throws MatrixException
     */
    public function process(NodeInterface $taskNode): string;
}