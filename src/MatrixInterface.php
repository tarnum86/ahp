<?php

namespace AHP;

use AHP\Matrix\ColumnInterface;
use AHP\Matrix\RowInterface;

/**
 * Interface MatrixInterface
 * Interface wrapping matrix to be used in AHP - inverse symmetric positive matrix
 * @package AHP
 */
interface MatrixInterface
{
    /**
     * @const array
     */
    public const MATRIX_SIZE_TO_RANDOM_CONSISTENCY_SCORE = [
        1 => 0,
        2 => 0,
        3 => 0.58,
        4 => 0.9,
        5 => 1.12,
        6 => 1.24,
        7 => 1.32,
        8 => 1.41,
        9 => 1.45,
        10 => 1.49,
        11 => 1.51,
        12 => 1.48,
        13 => 1.56,
        14 => 1.57,
        15 => 1.59,
    ];
    /**
     * @const array
     */
    public const OPINION_SCALE = [9, 7, 5, 3, 1, 1 / 3, 1 / 5, 1 / 7, 1 / 9];


    /**
     * @param RowInterface $row
     * @param bool $override
     * @return $this
     */
    public function addRow(RowInterface $row, bool $override = true): self;

    /**
     * @param ColumnInterface $column
     * @param bool $override
     * @return $this
     * @throws MatrixException
     */
    public function addColumn(ColumnInterface $column, bool $override = true): self;

    /**
     * @param array $rows
     * @param bool $createColumnsFromRows
     * @return $this
     */
    public function setRows(array $rows, bool $createColumnsFromRows = true): self;

    /**
     * @param string $rowIndex
     * @param string $columnIndex
     * @param float $value
     * @param bool $setInverseElement
     * @return $this
     * @throws MatrixException
     */
    public function setElement(string $rowIndex, string $columnIndex, float $value,
                               bool $setInverseElement = true): self;

    /**
     * @return int
     */
    public function getSize(): int;

    /**
     * @return array
     * @throws MatrixException
     */
    public function getColumns(): array;

    /**
     * @param string $columnIndex
     * @return ColumnInterface
     */
    public function getColumn(string $columnIndex): ColumnInterface;

    /**
     * @return array
     */
    public function getRows(): array;

    /**
     * @param string $rowIndex
     * @return RowInterface
     * @throws MatrixException
     */
    public function getRow(string $rowIndex): RowInterface;

    /**
     * @param string $rowIndex
     * @param string $columnIndex
     * @return float
     * @throws MatrixException
     */
    public function getElement(string $rowIndex, string $columnIndex): float;

    /**
     * @param string $rowIndex
     * @param string $columnIndex
     * @return float
     * @throws MatrixException
     */
    public function getInverseElement(string $rowIndex, string $columnIndex): float;

    /**
     * Creates columns based on rows in matrix
     */
    public function createColumnsFromRows(): void;

    /**
     * Creates rows based on columns in matrix
     */
    public function createRowsFromColumns(): void;
}