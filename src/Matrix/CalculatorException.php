<?php

namespace AHP\Matrix;

use \Exception;

/**
 * Class CalculatorException
 * @package AHP\Matrix
 */
class CalculatorException extends Exception {

}