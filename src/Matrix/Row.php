<?php

namespace AHP\Matrix;

/**
 * Class Row
 * Class wrapping matrix row
 * @package AHP
 */
class Row implements RowInterface
{
    /**
     * @var string
     */
    private string $index;
    /**
     * @var array
     */
    private array $elements;

    /**
     * Row constructor.
     * @param string $index
     * @param array $elements
     */
    public function __construct(string $index, array $elements = [])
    {
        $this->index = $index;
        $this->elements = $elements;
    }

    /**
     * @return string
     */
    public function getIndex(): string
    {
        return $this->index;
    }

    /**
     * @return array
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param string $index
     * @return float
     * @throws RowException
     */
    public function getElement(string $index): float
    {
        if (empty($this->elements[$index])) {
            throw new RowException(sprintf('No element with index %d in range.', $index));
        }
        return $this->elements[$index];
    }

    /**
     * @param string $index
     * @param float $number
     * @return $this
     */
    public function setElement(string $index, float $number): self
    {
        $this->elements[$index] = $number;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return count($this->elements);
    }
}