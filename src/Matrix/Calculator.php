<?php

namespace AHP\Matrix;

use AHP\Algorithm\AverageInterface;
use AHP\Algorithm\AverageInterface as AlgorithmAverageInterface;
use AHP\Matrix;
use AHP\MatrixException;
use AHP\MatrixInterface;
use AHP\NodeInterface;

/**
 * Class Calculator
 * @package AHP\Matrix
 */
class Calculator implements CalculatorInterface
{
    /**
     * @var AverageInterface
     */
    private AverageInterface $averageAlgorithm;

    /**
     * Calculator constructor.
     * @param AverageInterface|null $averageAlgorithm
     */
    public function __construct(AverageInterface $averageAlgorithm)
    {
        $this->setAverageAlgorithm($averageAlgorithm);
    }

    /**
     * @param AlgorithmAverageInterface $averageAlgorithm
     * @return $this
     */
    public function setAverageAlgorithm(AverageInterface $averageAlgorithm): self
    {
        $this->averageAlgorithm = $averageAlgorithm;
        return $this;
    }

    /**
     * @param RowInterface $row
     * @return float
     */
    public function getRowSum(RowInterface $row): float
    {
        return array_sum($row->getElements());
    }

    /**
     * @param RowInterface $row
     * @return float
     */
    public function getRowProduct(RowInterface $row): float
    {
        return array_product($row->getElements());
    }

    /**
     * @param RowInterface $row
     * @return float
     */
    public function getRowAverage(RowInterface $row): float
    {
        return $this->averageAlgorithm->calculate($row->getElements());
    }

    /**
     * @param MatrixInterface $matrixMultiplied
     * @param MatrixInterface $matrixMultiplier
     * @return MatrixInterface
     * @throws MatrixException
     * @throws RowException
     * @throws CalculatorException
     */
    public function multiplyMatrixes(MatrixInterface $matrixMultiplied, MatrixInterface $matrixMultiplier): MatrixInterface
    {
        if (empty($matrixMultiplied->getColumns())) {
            throw new CalculatorException('Matrix Multiplied has no columns.');
        }
        if (empty($matrixMultiplied->getRows())) {
            throw new CalculatorException('Matrix Multiplied has no rows.');
        }
        if (empty($matrixMultiplier->getColumns())) {
            throw new CalculatorException('Matrix Multiplier has no columns.');
        }
        if (empty($matrixMultiplier->getRows())) {
            throw new CalculatorException('Matrix Multiplier has no rows.');
        }
        if (count($matrixMultiplied->getColumns()) !== count($matrixMultiplier->getRows())) {
            throw new CalculatorException('Incompatible matrixes.');
        }
        $resultMatrix = new Matrix();
        /** @var RowInterface $row */
        foreach ($matrixMultiplied->getRows() as $row) {
            /** @var ColumnInterface $column */
            foreach ($matrixMultiplier->getColumns() as $column) {
                $result = 0;
                foreach ($row->getElements() as $key => $value) {
                    $result += $value * $column->getElement($key);
                }
                $resultMatrix->setElement($row->getIndex(), $column->getIndex(), $result);
            }
        }
        return $resultMatrix;
    }

    /**
     * @param MatrixInterface $matrix
     * @return float
     * @throws MatrixException
     */
    public function getConsistencyIndex(MatrixInterface $matrix): float
    {
        $maxValue = $this->getMatrixMaxValue($matrix);
        $matrixSize = $matrix->getSize();
        $consistencyIndex = ($maxValue - $matrixSize) / ($matrixSize - 1);
        return round($consistencyIndex, 2);
    }

    /**
     * @param int $matrixSize
     * @return float
     * @throws MatrixException
     */
    public function getRandomConsistencyScore(int $matrixSize): float
    {
        if (!isset(MatrixInterface::MATRIX_SIZE_TO_RANDOM_CONSISTENCY_SCORE[$matrixSize])) {
            throw new MatrixException('Matrix is too big to have random consistency score.');
        }

        return MatrixInterface::MATRIX_SIZE_TO_RANDOM_CONSISTENCY_SCORE[$matrixSize];
    }

    /**
     * @param MatrixInterface $matrix
     * @return float
     * @throws MatrixException
     */
    public function getConsistencyRatio(MatrixInterface $matrix): float
    {
        $randomConsistencyScore = $this->getRandomConsistencyScore($matrix->getSize());
        if ($randomConsistencyScore === 0.0) {
            return 0.0;
        }
        $consistencyIndex = $this->getConsistencyIndex($matrix);
        return round($consistencyIndex / $randomConsistencyScore, 2);
    }

    /**
     * @param MatrixInterface $matrix
     * @return float
     * @throws MatrixException
     */
    public function getMatrixMaxValue(MatrixInterface $matrix): float
    {
        $maxValue = 0;
        foreach ($matrix->getColumns() as $key => $column) {
            $columnSum = $this->getRowSum($column);
            $correspondingRow = $matrix->getRow($key);
            $correspondingRowNVP =
                $this->getRowNVP($matrix, $correspondingRow);
            $maxValue += $columnSum * $correspondingRowNVP;
        }
        return round($maxValue, 2);
    }

    /**
     * @param MatrixInterface $matrix
     * @param RowInterface $row
     * @return float
     */
    public function getRowNVP(MatrixInterface $matrix, RowInterface $row): float
    {
        $rowsTotalAverage = 0;
        /** @var Row $row */
        foreach ($matrix->getRows() as $matrixRow) {
            $rowsTotalAverage += $this->getRowAverage($matrixRow);
        }
        return $this->getRowAverage($row) / $rowsTotalAverage;
    }

    /**
     * @param MatrixInterface $matrix
     * @param string $nvpIndexName
     * @return MatrixInterface
     * @throws MatrixException
     * @throws CalculatorException
     */
    public function getMatrixNVPMatrix(MatrixInterface $matrix, string $nvpIndexName): MatrixInterface
    {
        $resultMatrix = new Matrix();
        foreach ($matrix->getRows() as $rowIndex => $row) {
            $rowNVP = $this->getRowNVP($matrix, $row);
            $resultMatrix->setElement($rowIndex, $nvpIndexName, $rowNVP);
        }
        $nvpSum = $this->getRowSum($resultMatrix->getColumn($nvpIndexName));
        if (abs($nvpSum - ValidatorInterface::NORMAL_NVP_SUM)
            > ValidatorInterface::MAX_ALLOWED_DELTA_TO_NORMAL_NVP_SUM) {
            throw new CalculatorException(
                'Difference between sum of nodes priorities vectors and "1" must not be more than %f.',
                ValidatorInterface::MAX_ALLOWED_DELTA_TO_NORMAL_NVP_SUM
            );
        }


        return $resultMatrix;
    }

    /**
     * @param NodeInterface $node
     * @return MatrixInterface
     * @throws MatrixException
     * @throws RowException
     * @throws CalculatorException
     */
    public function getNodeNVPMatrixRecursively(NodeInterface $node): MatrixInterface
    {
        // gets current node matrix nvp matrix
        $nodeNVPMatrix = $this->getMatrixNVPMatrix($node->getMatrix(), $node->getName());
        //foreach subnodes and gets subnode nvp matrix (when node has no subnodes, it stops)
        $subNodesTree = $node->getSubNodesTree();
        if (empty($subNodesTree)) {
            return $nodeNVPMatrix;
        }
        $subNodesNVPMatrix = new Matrix();
        /** @var NodeInterface $subNode */
        foreach ($node->getSubNodesTree() as $subNode) {
            // add each subnode nvp matrix as an additional column to resulting subnodes nvp matrix
            $subNodeNVPMatrix = $this->getNodeNVPMatrixRecursively($subNode);
            $subNodesNVPMatrix->addColumn($subNodeNVPMatrix->getColumn($subNode->getName()));
        }
        $subNodesNVPMatrix->createRowsFromColumns();
        // multiply current node matrix nvp matrix (step a) to resulting subnodes nvp matrix (step c)
        $resultMatrix = $this->multiplyMatrixes($subNodesNVPMatrix, $nodeNVPMatrix);
        // result always must be 1 column matrix
        return $resultMatrix;
    }

    /**
     * @param NodeInterface $node
     * @return string
     * @throws CalculatorException
     * @throws MatrixException
     * @throws RowException
     */
    public function getNodeBestAlternative(NodeInterface $node): string
    {
        $nvpMatrix = $this->getNodeNVPMatrixRecursively($node);
        $resultNVPs = $nvpMatrix->getColumn($node->getName())->getElements();
        arsort($resultNVPs);
        return array_key_first($resultNVPs);
    }
}