<?php

namespace AHP\Matrix;

use AHP\MatrixInterface;

/**
 * Interface ValidatorInterface
 * @package AHP\Matrix
 */
interface ValidatorInterface
{
    /**
     * @const float
     */
    public const MAX_ALLOWED_DELTA_TO_NORMAL_NVP_SUM = 0.02;
    /**
     * @const float
     */
    public const MATRIX_MAX_CONSISTENCY_RATIO = 0.15;
    /**
     * @const int
     */
    public const NORMAL_NVP_SUM = 1;

    /**
     * @param MatrixInterface $matrix
     * @throws ValidatorException
     */
    public function validate(MatrixInterface $matrix): void;
}