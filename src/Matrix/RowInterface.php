<?php

namespace AHP\Matrix;

/**
 * Interface RowInterface
 * @package AHP\Matrix
 */
interface RowInterface
{
    /**
     * @return string
     */
    public function getIndex(): string;

    /**
     * @return array
     */
    public function getElements(): array;

    /**
     * @param string $index
     * @return float
     * @throws RowException
     */
    public function getElement(string $index): float;

    /**
     * @param string $index
     * @param float $number
     * @return $this
     */
    public function setElement(string $index, float $number): self;

    /**
     * @return int
     */
    public function getQuantity(): int;
}