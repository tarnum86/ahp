<?php

namespace AHP\Matrix;

/**
 * Interface ColumnInterface
 * @package AHP\Matrix
 */
interface ColumnInterface extends RowInterface
{
}