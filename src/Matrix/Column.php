<?php

namespace AHP\Matrix;

/**
 * Class Column
 * Class wrapping matrix column
 * @package AHP
 */
class Column extends Row implements ColumnInterface
{
    /**
     * @param string $index
     * @param float $number
     * @return $this
     */
    public function setElement(string $index, float $number): self
    {
        parent::setElement($index, $number);
        return $this;
    }
}