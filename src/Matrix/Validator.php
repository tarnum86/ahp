<?php

namespace AHP\Matrix;

use AHP\MatrixException;
use AHP\MatrixInterface;

/**
 * Class Validator
 * @package AHP\Matrix
 */
class Validator implements ValidatorInterface
{
    /**
     * @var CalculatorInterface
     */
    private CalculatorInterface $calculator;

    /**
     * Validator constructor.
     * @param CalculatorInterface $calculator
     */
    public function __construct(CalculatorInterface $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @param MatrixInterface $matrix
     * @throws ValidatorException
     */
    public function validate(MatrixInterface $matrix): void
    {
        if ($matrix->getSize() === 0) {
            throw new ValidatorException('Matrix must be not empty!');
        }
        $rowsNumber = count($matrix->getRows());
        /** @var Row $row */
        foreach ($matrix->getRows() as $rowIndex => $row) {
            if (!$row instanceof Row) {
                throw new ValidatorException('All rows in matrix must be type of Row class.');
            }
            // matrix is square - equal number of rows and columns - in our simplified case
            $columnsNumber = count($row->getElements());
            if ($columnsNumber === 0) {
                throw new ValidatorException('Matrix must have at least 1 column!');
            }
            if ($columnsNumber !== $rowsNumber) {
                throw new ValidatorException('Matrix must be square.');
            }
            $rowNumbers = $row->getElements();
            foreach ($rowNumbers as $columnIndex => $number) {
                // matrix must have positive numbers
                if ($number < 0) {
                    throw new ValidatorException('Matrix must have all numbers positive.');
                }
                // its inverse symmetric matrix -  all elements of the main diagonal of such a matrix are equal to 1, and
                // a ki = 1 / a ik (i - number of row, k - number of column)  OR a ki * a ik = = 1
                try {
                    $inverseElement = $matrix->getInverseElement($rowIndex, $columnIndex);
                } catch (MatrixException $exception) {
                    throw new ValidatorException($exception);
                }
                if ($number != (1 / $inverseElement)) {
                    throw new ValidatorException('Matrix must be inverse and symmetric.');
                }
            }
        }
        try {
            $matrixConsistencyRatio = $this->calculator->getConsistencyRatio($matrix);
        } catch (MatrixException $exception) {
            throw new ValidatorException($exception);
        }
        if ($matrixConsistencyRatio > ValidatorInterface::MATRIX_MAX_CONSISTENCY_RATIO) {
            throw new ValidatorException(
                sprintf('Matrix consistency ratio must be less than %.2f.', ValidatorInterface::MATRIX_MAX_CONSISTENCY_RATIO)
            );
        }
    }
}