<?php

namespace AHP\Matrix;

use AHP\Algorithm\AverageInterface as AlgorithmAverageInterface;
use AHP\{MatrixException, MatrixInterface, NodeInterface};

/**
 * Interface CalculatorInterface
 * @package AHP\Matrix
 */
interface CalculatorInterface
{
    /**
     * @param AlgorithmAverageInterface $averageCalculationAlgorithm
     * @return $this
     */
    public function setAverageAlgorithm(AlgorithmAverageInterface $averageCalculationAlgorithm): self;

    /**
     * @param RowInterface $row
     * @return float
     */
    public function getRowSum(RowInterface $row): float;

    /**
     * @param RowInterface $row
     * @return float
     */
    public function getRowProduct(RowInterface $row): float;

    /**
     * @param RowInterface $row
     * @return float
     */
    public function getRowAverage(RowInterface $row): float;

    /**
     * @param MatrixInterface $matrixMultiplied
     * @param MatrixInterface $matrixMultiplier
     * @return MatrixInterface
     */
    public function multiplyMatrixes(MatrixInterface $matrixMultiplied,
                                     MatrixInterface $matrixMultiplier): MatrixInterface;

    /**
     * @param MatrixInterface $matrix
     * @return float
     * @throws MatrixException
     */
    public function getConsistencyIndex(MatrixInterface $matrix): float;

    /**
     * @param int $matrixSize
     * @return float
     * @throws MatrixException
     */
    public function getRandomConsistencyScore(int $matrixSize): float;

    /**
     * @param MatrixInterface $matrix
     * @return float
     * @throws MatrixException
     */
    public function getConsistencyRatio(MatrixInterface $matrix): float;

    /**
     * @param MatrixInterface $matrix
     * @return float
     * @throws MatrixException
     */
    public function getMatrixMaxValue(MatrixInterface $matrix): float;

    /**
     * @param MatrixInterface $matrix
     * @param Row $row
     * @return float
     */
    public function getRowNVP(MatrixInterface $matrix, Row $row): float;

    /**
     * @param MatrixInterface $matrix
     * @param string $nvpIndexName
     * @return MatrixInterface
     */
    public function getMatrixNVPMatrix(MatrixInterface $matrix, string $nvpIndexName): MatrixInterface;

    /**
     * @param NodeInterface $node
     * @return MatrixInterface
     * @throws MatrixException
     */
    public function getNodeNVPMatrixRecursively(NodeInterface $node): MatrixInterface;

    /**
     * @param NodeInterface $node
     * @return string
     */
    public function getNodeBestAlternative(NodeInterface $node): string;
}