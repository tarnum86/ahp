<?php

namespace AHP;

/**
 * Interface NodeInterface
 * @package AHP
 */
interface NodeInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return Matrix
     */
    public function getMatrix(): MatrixInterface;

    /**
     * @return array
     */
    public function getSubNodesTree(): array;

    /**
     * @return string
     */
    public function getDescription(): string;
}