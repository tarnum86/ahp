<?php

namespace AHP;

use AHP\Matrix\CalculatorException;
use AHP\Matrix\CalculatorInterface;
use AHP\Matrix\ValidatorException as MatrixValidatorException;
use AHP\Node\ValidatorException as NodeValidatorException;
use AHP\Node\ValidatorInterface;
use \Exception;

/**
 * Class Task
 * @package AHP
 */
class Processor implements ProcessorInterface
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;
    /**
     * @var CalculatorInterface
     */
    private CalculatorInterface $calculator;

    /**
     * Processor constructor.
     * @param ValidatorInterface $validator
     * @param CalculatorInterface $calculator
     */
    public function __construct(ValidatorInterface $validator, CalculatorInterface $calculator)
    {
        $this->validator = $validator;
        $this->calculator = $calculator;
    }

    /**
     * @param NodeInterface $node
     * @return string
     * @throws ProcessorException
     */
    public function process(NodeInterface $node): string
    {
        try {
            $this->validator->validate($node);
            return $this->calculator->getNodeBestAlternative($node);
        } catch (NodeValidatorException $exception) {
            throw new ProcessorException(
                sprintf('Node with name "%s" is not valid. %s', $node->getName(), $exception->getMessage())
            );
        } catch (MatrixValidatorException $exception) {
            throw new ProcessorException(
                sprintf('Matrix is not valid inside node "%s". %s', $node->getName(), $exception->getMessage())
            );
        } catch (CalculatorException $exception) {
            throw new ProcessorException(
                sprintf('Calculations error inside node "%s". %s', $node->getName(), $exception->getMessage())
            );
        } catch (Exception $exception) {
            throw new ProcessorException(sprintf('Something went wrong. %s', $exception->getMessage()));
        }
    }
}