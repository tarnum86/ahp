<?php

namespace AHP\Node;

use AHP\Matrix\CalculatorException;
use AHP\NodeInterface;
use AHP\Matrix\ValidatorException as MatrixValidatorException;
/**
 * Interface ValidatorInterface
 * @package AHP\Node
 */
interface ValidatorInterface
{
    /**
     * @param NodeInterface $node
     * @throws ValidatorException|MatrixValidatorException|CalculatorException
     */
    public function validate(NodeInterface $node): void;
}