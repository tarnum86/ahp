<?php

namespace AHP\Node;

use \Exception;

/**
 * Class ValidatorException
 * @package AHP\Node
 */
class ValidatorException extends Exception {

}