<?php

namespace AHP\Node;

use AHP\Matrix\CalculatorInterface;
use AHP\NodeInterface;
use AHP\Matrix\ValidatorException as MatrixValidatorException;
use AHP\Matrix\ValidatorInterface as MatrixValidatorInterface;
use AHP\Node\ValidatorInterface as NodeValidatorInterface;

/**
 * Class Validator
 * @package AHP\Node
 */
class Validator implements NodeValidatorInterface
{
    /**
     * @var CalculatorInterface
     */
    private CalculatorInterface $calculator;
    /**
     * @var MatrixValidatorInterface
     */
    private MatrixValidatorInterface $matrixValidator;

    /**
     * Validator constructor.
     * @param CalculatorInterface $calculator
     * @param MatrixValidatorInterface $matrixValidator
     */
    public function __construct(CalculatorInterface $calculator,
                                MatrixValidatorInterface $matrixValidator)
    {
        $this->calculator = $calculator;
        $this->matrixValidator = $matrixValidator;
    }

    /**
     * @param NodeInterface $node
     * @throws ValidatorException
     */
    public function validate(NodeInterface $node): void
    {
        if (!empty($node->getSubNodesTree()) && ($node->getMatrix()->getSize() != count($node->getSubNodesTree()))) {
            throw new ValidatorException(
                'Size of sub nodes matrix must be equal to quantity of top level matrixes in sub nodes tree.'
            );
        }
        try {
            $this->matrixValidator->validate($node->getMatrix());
        } catch (MatrixValidatorException $exception) {
            throw new ValidatorException(sprintf('Matrix is not valid. %s', $exception->getMessage()));
        }
    }
}