<?php

namespace AHP;

/**
 * Class Node
 * @package AHP
 */
class Node implements NodeInterface
{
    /**
     * @var string
     */
    private string $name;
    /**
     * @var string
     */
    private string $description;
    /**
     * @var MatrixInterface
     */
    private MatrixInterface $subNodesMatrix;
    /**
     * @var array
     */
    private array $subNodesTree;

    /**
     * NodeAbstract constructor.
     * @param string $name
     * @param MatrixInterface $subNodesMatrix
     * @param array $subNodesTree
     * @param string $description
     */
    public function __construct(string $name, MatrixInterface $subNodesMatrix, array $subNodesTree = [],
                                string $description = '')
    {
        $this->name = $name;
        $this->subNodesMatrix = $subNodesMatrix;
        $this->subNodesTree = $subNodesTree;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Matrix
     */
    public function getMatrix(): MatrixInterface
    {
        return $this->subNodesMatrix;
    }

    /**
     * @return array
     */
    public function getSubNodesTree(): array
    {
        return $this->subNodesTree;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}