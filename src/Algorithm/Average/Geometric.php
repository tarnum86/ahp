<?php

namespace AHP\Algorithm\Average;

use AHP\Algorithm\AverageInterface;

/**
 * Class Geometric
 * @package AHP\Algorithm\Average
 */
class Geometric implements AverageInterface
{
    /**
     * @param array $numbers
     * @return float
     */
    public function calculate(array $numbers): float
    {
        return array_product($numbers) ** (1 / count($numbers));
    }
}