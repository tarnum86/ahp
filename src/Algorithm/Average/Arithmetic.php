<?php

namespace AHP\Algorithm\Average;

use AHP\Algorithm\AverageInterface;

/**
 * Class Arithmetic
 * @package AHP\Algorithm\Average
 */
class Arithmetic implements AverageInterface
{
    /**
     * @param array $numbers
     * @return float
     */
    public function calculate(array $numbers): float
    {
        return array_sum($numbers) / count($numbers);
    }
}