<?php

namespace AHP\Algorithm;

/**
 * Interface AverageInterface
 * @package AHP\Algorithm
 */
interface AverageInterface
{
    /**
     * @param array $numbers
     * @return float
     */
    public function calculate(array $numbers): float;
}