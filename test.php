<?php
namespace AHP\Test\Integration;

use AHP\{Processor, Node, Matrix};
use AHP\Matrix\{Calculator, Validator as MatrixValidator, Row};
use AHP\Node\{Validator as NodeValidator};
use AHP\Algorithm\Average\{Geometric, Arithmetic};

define('BASE_PATH', getcwd());

require_once BASE_PATH . DIRECTORY_SEPARATOR . 'vendor/autoload.php';

/**
 * SIMPLE CASE  - task -> criterias -> alternatives
 */
$goal = 'Decide what car to buy';
$criteria1 = 'price';
$criteria2 = 'quality';
$alternative1 = 'Volkswagen Golf';
$alternative2 = 'Toyota Corolla';

$nodeMatrix = new Matrix([
    new Row($criteria1, [$criteria1 => 1, $criteria2 => 2]),
    new Row($criteria2, [$criteria1 => 1 / 2, $criteria2 => 1]),
]);
$subNodesTree = [
    new Node($criteria1,
        new Matrix([
            new Row($alternative1, [$alternative1 => 1, $alternative2 => 3]),
            new Row($alternative2, [$alternative1 => 1 / 3, $alternative2 => 1]),
        ])
    ),
    new Node($criteria2,
        new Matrix([
            new Row($alternative1, [$alternative1 => 1, $alternative2 => 5]),
            new Row($alternative2, [$alternative1 => 1 / 5, $alternative2 => 1]),
        ])
    )
];
$task = new Node($goal, $nodeMatrix, $subNodesTree);
$calculator = new Calculator(new Geometric());
$nodeValidator = new NodeValidator($calculator, new MatrixValidator($calculator));
$processor = new Processor($nodeValidator, $calculator);
$bestAlternative = $processor->process($task);
/**
 * Volkswagen Golf , as price factor is more important(twice) ,
 * and both price ("3" preference) and quality ("5" preference)
 * are better than for 'Toyota Corolla'
 */
echo 'Best Alternative is: ' . $bestAlternative . PHP_EOL;

/** 1 criteria case */
$criteria1 = 'price';
$alternative1 = 'Volkswagen Golf';
$alternative2 = 'Toyota Corolla';

$task = new Node('Decide what car to buy',
    new Matrix([
        $criteria1 => new Row($criteria1, [$criteria1 => 1]),
    ]),
    [
        new Node($criteria1,
            new Matrix([
                new Row($alternative1, [$alternative1 => 1, $alternative2 => 3]),
                new Row($alternative2, [$alternative1 => 1 / 3, $alternative2 => 1]),
            ])
        )
    ]);

$calculator = new Calculator(new Geometric());
$nodeValidator = new NodeValidator($calculator, new MatrixValidator($calculator));
$processor = new Processor($nodeValidator, $calculator);
$bestAlternative = $processor->process($task);

// Volkswagen Golf , as it's price ("3" preference) is better than for 'Toyota Corolla'
echo 'Best Alternative is: ' . $bestAlternative . PHP_EOL;


// 1 criteria case and 1 alternative
$criteria1 = 'price';
$alternative1 = 'Volkswagen Golf';

$task = new Node('Decide what car to buy',
    new Matrix([
        $criteria1 => new Row($criteria1, [$criteria1 => 1]),
    ]),
    [
        new Node($criteria1, new Matrix([
                new Row($alternative1, [$alternative1 => 1]),
            ])
        )
    ]);
$calculator = new Calculator(new Geometric());
$nodeValidator = new NodeValidator($calculator, new MatrixValidator($calculator));
$processor = new Processor($nodeValidator, $calculator);
$bestAlternative = $processor->process($task);
/** Volkswagen Golf is only one alternative */
echo 'Best Alternative is: ' . $bestAlternative . PHP_EOL;
//
//
/** 2 criterias, 1 alternative */
$criteria1 = 'price';
$criteria2 = 'quality';
$alternative2 = 'Toyota Corolla';

$task = new Node('Decide what car to buy',
    new Matrix([
        new Row($criteria1, [$criteria1 => 1, $criteria2 => 2]),
        new Row($criteria2, [$criteria1 => 1 / 2, $criteria2 => 1]),
    ]),
    [
        new Node($criteria1, new Matrix([
                new Row($alternative2, [$alternative2 => 1]),
            ])
        ),
        new Node($criteria2, new Matrix([
                new Row($alternative2, [$alternative2 => 1]),
            ])
        )
    ]);
$calculator = new Calculator(new Arithmetic());
$nodeValidator = new NodeValidator($calculator, new MatrixValidator($calculator));
$processor = new Processor($nodeValidator, $calculator);
$bestAlternative = $processor->process($task);

/** Toyota Corolla is only one alternative */
//echo 'Best Alternative is: ' . $bestAlternative . PHP_EOL;

/** Usage when complex case  task -> professionals -> criterias -> alternatives */

$taskName = 'make diagnosis';

$specialist1 = 'S1';
$specialist2 = 'S2';
$specialist3 = 'S3';

$criteria1 = 'C1';
$criteria2 = 'C2';
$criteria3 = 'C3';

$decision1 = 'D1';
$decision2 = 'D2';
$decision3 = 'D3';
$decision4 = 'D4';


$task = new Node($taskName,
    new Matrix([
        new Row($specialist1, [$specialist1 => 1, $specialist2 => 1, $specialist3 => 5]),
        new Row($specialist2, [$specialist1 => 1, $specialist2 => 1, $specialist3 => 5]),
        new Row($specialist3, [$specialist1 => 1 / 5, $specialist2 => 1 / 5, $specialist3 => 1]),
    ]),

    [
        new Node($specialist1,
            new Matrix([
                new Row($criteria1, [$criteria1 => 1, $criteria2 => 5]),
                new Row($criteria2, [$criteria2 => 1 / 5, $criteria1 => 1]),
            ]),
            [
                new Node($criteria1,
                    new Matrix([
                        new Row($decision1, [$decision1 => 1, $decision2 => 3, $decision3 => 1, $decision4 => 1 / 5]),
                        new Row($decision2, [$decision1 => 1 / 3, $decision2 => 1, $decision3 => 1 / 3, $decision4 => 1 / 7]),
                        new Row($decision3, [$decision1 => 1, $decision2 => 3, $decision3 => 1, $decision4 => 1 / 5]),
                        new Row($decision4, [$decision1 => 5, $decision2 => 7, $decision3 => 5, $decision4 => 1])
                    ])
                ),
                new Node($criteria2,
                    new Matrix([
                        new Row($decision1, [$decision1 => 1, $decision2 => 1 / 5, $decision3 => 1, $decision4 => 1]),
                        new Row($decision2, [$decision1 => 5, $decision2 => 1, $decision3 => 5, $decision4 => 5]),
                        new Row($decision3, [$decision1 => 1, $decision2 => 1 / 5, $decision3 => 1, $decision4 => 1]),
                        new Row($decision4, [$decision1 => 1, $decision2 => 1 / 5, $decision3 => 1, $decision4 => 1]),
                    ])
                )
            ]
        ),
        new Node($specialist2,
            new Matrix([
                new Row($criteria2, [$criteria2 => 1, $criteria3 => 1 / 5]),
                new Row($criteria3, [$criteria2 => 5, $criteria3 => 1]),
            ]),
            [
                new Node($criteria2, new Matrix([
                        new Row($decision1, [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1]),
                        new Row($decision2, [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1]),
                        new Row($decision3, [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1]),
                        new Row($decision4, [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1]),
                    ])
                ),
                new Node($criteria3, new Matrix([
                        new Row($decision1, [$decision1 => 1, $decision2 => 1, $decision3 => 1 / 5, $decision4 => 1]),
                        new Row($decision2, [$decision1 => 1, $decision2 => 1, $decision3 => 1 / 5, $decision4 => 1]),
                        new Row($decision3, [$decision1 => 5, $decision2 => 5, $decision3 => 1, $decision4 => 5]),
                        new Row($decision4, [$decision1 => 1, $decision2 => 1, $decision3 => 1 / 5, $decision4 => 1]),
                    ])
                )
            ]
        ),
        new Node($specialist3,
            new Matrix([
                new Row($criteria3, [$criteria3 => 1])
            ]),
            [
                new Node($criteria3, new Matrix([
                        new Row($decision1, [$decision1 => 1, $decision2 => 1, $decision3 => 3, $decision4 => 1]),
                        new Row($decision2, [$decision1 => 1, $decision2 => 1, $decision3 => 3, $decision4 => 1]),
                        new Row($decision3, [$decision1 => 1 / 3, $decision2 => 1 / 3, $decision3 => 1, $decision4 => 1 / 3]),
                        new Row($decision4, [$decision1 => 1, $decision2 => 1, $decision3 => 3, $decision4 => 1]),
                    ])
                )
            ]
        )
    ]
);

$calculator = new Calculator(new Arithmetic());
$nodeValidator = new NodeValidator($calculator, new MatrixValidator($calculator));
$processor = new Processor($nodeValidator, $calculator);
$bestAlternative = $processor->process($task);

/** Best diagnosis is : D3 */
echo 'Best diagnosis is : ' . $bestAlternative . PHP_EOL;