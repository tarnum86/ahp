<?php

namespace AHP\Test\Helper;

use AHP\Matrix;
use AHP\Matrix\RowInterface;
use AHP\Matrix\ColumnInterface;
use AHP\MatrixInterface;
use AHP\NodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class MatrixMocker
 * It helps generating matrix mock for others test classes in this library
 *
 * @package AHP\Test\Helper
 */
class Mocker extends TestCase
{
    /**
     * Prepare matrix by passing rows array like:
     * [
     *      'A' => ['A' => 1, 'B' => 2 ],
     *      'B' => ['A' => 1/2, 'B' => 1 ]
     * ]
     * and will transform it into matrix like
     * Matrix([
     *    'A' => new Row('A', ['A' => 1, 'B' => 2]),
     *    'B' => new Row('B', ['A' => 1 / 2, 'B' => 1]),
     * ]),
     * @param array|null $rows
     * @return MatrixInterface
     * @throws \Exception
     */
    public function prepareMatrix(array $rows = null): MatrixInterface
    {
//        $matrixMock = TestCase::createMock(Matrix::class);
        $matrixMock = $this->getMockBuilder(Matrix::class)->getMock();
        if (empty($rows)) {
            return $matrixMock;
        }
        $rowsMocksPrepared = $this->prepareRowsMocks($rows, $matrixMock);
        $columns = $this->transformRowsIntoColumns($rows);
        $columnsPrepared = $this->prepareColumnsMocks($columns, $matrixMock);

        $matrixMock->expects($this->any())->method('getRows')
            ->will(static::returnValue($rowsMocksPrepared));
        $matrixMock->expects($this->any())->method('getColumns')
            ->will(static::returnValue($columnsPrepared));
        if (!empty($size)) {
            $matrixMock->expects($this->any())->method('getSize')
                ->will(static::returnValue(count($rows)));
        }

        return $matrixMock;
    }

    /**
     * Transforms array of simplified rows
     * [
     *      'A' => ['A' => 1, 'B' => 2 ],
     *      'B' => ['A' => 1/2, 'B' => 1 ]
     * ] into columns:
     * [
     *      'A' => ['A' => 1, 'B' => 1/2 ],
     *      'B' => ['A' => 2, 'B' => 1 ]
     * ]
     * @param array $rows
     * @return array
     */
    private function transformRowsIntoColumns(array $rows): array
    {
        $columns = [];
        foreach ($rows as $rowIndex => $rowValues) {
            foreach ($rowValues as $columnIndex => $number) {
                if (empty($columns[$columnIndex])) {
                    $columns[$columnIndex] = [];
                }
                $columns[$columnIndex][$rowIndex] = $number;
            }
        }
        return $columns;
    }

    /**
     * @param array $rows
     * @param MockObject $matrixMock
     * @return array
     * @throws \Exception
     */
    private function prepareRowsMocks(array $rows, MockObject $matrixMock): array
    {
        $rowsPrepared = [];
        $getRowMap = [];
        foreach ($rows as $rowIndex => $rowElements) {
            if (!is_array($rowElements)) {
                throw new \Exception('Row elements is not an array!');
            }
//            $rowMock = $this->createMock(RowInterface::class);
            $rowMock = $this->getMockBuilder(RowInterface::class)->getMock();
            $rowMock->expects($this->any())->method('getElements')
                ->will($this->returnValue($rowElements));
            $rowMock->expects($this->any())->method('getIndex')
                ->will($this->returnValue($rowIndex));
            $rowMock->expects($this->any())->method('getQuantity')
                ->will($this->returnValue(count($rowElements)));
            $rowGetElementMap = [];
            foreach ($rowElements as $rowElementIndex => $rowElementValue) {
                $rowGetElementMap[] = [$rowElementIndex => $rowElementValue];
            }
            $rowMock->expects($this->any())->method('getElement')->will(static::returnValueMap($rowGetElementMap));
            $rowsPrepared[$rowIndex] = $rowMock;
            $getRowMap[] = [$rowIndex => $rowMock];
        }
        $matrixMock->expects($this->any())->method('getRow')
            ->will(static::returnValueMap($getRowMap));

        return $rowsPrepared;
    }

    /**
     * @param array $columns
     * @param MockObject $matrixMock
     * @return array
     * @throws \Exception
     */
    private function prepareColumnsMocks(array $columns, MockObject $matrixMock): array
    {
        $columnsPrepared = [];
        $getColumnMap = [];
        foreach ($columns as $columnIndex => $columnElements) {
            if (!is_array($columnElements)) {
                throw new \Exception('Row elements is not an array!');
            }
            $columnMock = $this->getMockBuilder(ColumnInterface::class)->getMock();
            $columnMock->expects($this->any())->method('getElements')
                ->will($this->returnValue($columnElements));
            $columnMock->expects($this->any())->method('getIndex')
                ->will($this->returnValue($columnIndex));
            $columnMock->expects($this->any())->method('getQuantity')
                ->will($this->returnValue(count($columnElements)));
            $columnGetElementMap = [];
            foreach ($columnElements as $columnElementIndex => $columnElementValue) {
                $columnGetElementMap[] = [$columnElementIndex, $columnElementValue];
            }
            $columnMock->expects($this->any())->method('getElement')->will(static::returnValueMap($columnGetElementMap));
            $columnsPrepared[$columnIndex] = $columnMock;
            $getColumnMap[] = [$columnIndex, $columnMock];
        }
        $matrixMock->expects($this->any())->method('getColumn')
            ->will(static::returnValueMap($getColumnMap));

        return $columnsPrepared;
    }

    /**
     * @param string $name
     * @param MatrixInterface $subNodesMatrix
     * @param array $subNodesTree
     * @param string $description
     * @return NodeInterface
     */
    public function prepareNode(string $name, MatrixInterface $subNodesMatrix, array $subNodesTree = [],
                                string $description = ''): NodeInterface
    {
//        $node = $this->createMock(Node::class);
        $node = $this->getMockBuilder(NodeInterface::class)->getMock();
        $node->expects($this->any())->method('getName')
            ->will(static::returnValue($name));
        $node->expects($this->any())->method('getMatrix')
            ->will(static::returnValue($subNodesMatrix));
        $node->expects($this->any())->method('getSubNodesTree')
            ->will(static::returnValue($subNodesTree));
        $node->expects($this->any())->method('getDescription')
            ->will(static::returnValue($description));

        return $node;
    }

    /**
     * Dummy test method to avoid warning about no tests found
     */
    public function testDummyToAvoidWarningNoTestsFound()
    {
        $this->assertTrue(true);
    }
}
