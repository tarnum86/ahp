<?php

namespace AHP\Test;

use AHP\Node\ValidatorInterface;
use AHP\Matrix\CalculatorInterface;
use AHP\NodeInterface;
use AHP\Processor;
use AHP\Test\Helper\Mocker;
use PHPUnit\Framework\TestCase;

/**
 * Class ProcessorTest
 * @package AHP\Test
 */
class ProcessorTest extends TestCase
{
    /**
     * @var Mocker
     */
    private Mocker $mocker;

    /**
     * setup
     */
    public function setUp(): void
    {
        $this->mocker = new Mocker();
    }

    /**
     * @covers \AHP\Processor::process
     */
    public function testProcessNoExceptions()
    {
        $expectedResult = 'X';
        $validatorMock = $this->createMock(ValidatorInterface::class);
        $calculatorMock = $this->createMock(CalculatorInterface::class);
        $calculatorMock->expects($this->any())->method('getNodeBestAlternative')
            ->will(static::returnValue($expectedResult));
        $nodeMock = $this->createMock(NodeInterface::class);
        $testedInstance = new Processor($validatorMock, $calculatorMock);
        static::assertEquals($expectedResult, $testedInstance->process($nodeMock));
    }
}
