<?php

namespace AHP\Test\Algorithm;

use AHP\Algorithm\Average\Geometric;
use PHPUnit\Framework\TestCase;

/**
 * Class GeometricTest
 * @package AHP\Test\Matrix
 */
class GeometricTest extends TestCase
{
    /**
     * @covers \AHP\Algorithm\Average\Geometric::calculate
     */
    public function testCalculate()
    {
        $testedInstance = new Geometric();
        static::assertEqualsWithDelta(0.55, $testedInstance->calculate([1, 1 / 2, 1 / 3]), 0.01);

        $testedInstance = new Geometric();
        static::assertEqualsWithDelta(1.82, $testedInstance->calculate([3, 2, 1]), 0.01);
    }
}