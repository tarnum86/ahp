<?php

namespace AHP\Test\Algorithm;

use AHP\Algorithm\Average\Arithmetic;
use PHPUnit\Framework\TestCase;

/**
 * Class ArithmeticTest
 * @package AHP\Test\Matrix
 */
class ArithmeticTest extends TestCase
{
    /**
     * @covers \AHP\Algorithm\Average\Arithmetic::calculate
     */
    public function testCalculate()
    {
        $testedInstance = new Arithmetic();
        static::assertEqualsWithDelta(0.61, $testedInstance->calculate([1, 1 / 2, 1 / 3]), 0.01);

        $testedInstance = new Arithmetic();
        static::assertEqualsWithDelta(2, $testedInstance->calculate([3, 2, 1]), 0.01);
    }
}