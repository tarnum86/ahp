<?php

namespace AHP\Test\Matrix;

use AHP\Algorithm\Average\Arithmetic;
use AHP\Algorithm\Average\Geometric;
use AHP\Matrix;
use AHP\Matrix\Row;
use AHP\Matrix\Column;
use AHP\Matrix\Calculator;
use AHP\Processor;
use AHP\Test\Helper\Mocker;
use PHPUnit\Framework\TestCase;

/**
 * Class Calculator
 * @package AHP\Test\Matrix
 */
class CalculatorTest extends TestCase
{
    /**
     * @var Mocker
     */
    private Mocker $mocker;

    /**
     * setup
     */
    public function setUp(): void
    {
        $this->mocker = new Mocker();
    }

    /**
     * @covers \AHP\Matrix\Calculator::getMatrixNVPMatrix
     */
    public function testGetMatrixNVPMatrix()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $criteria4 = 'D';
        $nvpIndexName = 'NVP_test_index_name';
        $matrixMock = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 3, $criteria4 => 2],
            $criteria2 => [$criteria1 => 2, $criteria2 => 1, $criteria3 => 3, $criteria4 => 1],
            $criteria3 => [$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1, $criteria4 => 2],
            $criteria4 => [$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 2, $criteria4 => 1]
        ]);
        $testedInstance = new Calculator(new Geometric());
        $expectedResult = new Matrix([
            $criteria1 => new Row($criteria1, [$nvpIndexName => 0.30786405406547807]),
            $criteria2 => new Row($criteria2, [$nvpIndexName => 0.36611412354824896]),
            $criteria3 => new Row($criteria3, [$nvpIndexName => 0.16061109459224288]),
            $criteria4 => new Row($criteria4, [$nvpIndexName => 0.16541072779403002]),
        ]);
        $actualResult = $testedInstance->getMatrixNVPMatrix($matrixMock, $nvpIndexName);
        static::assertEquals($expectedResult, $actualResult);
    }

    /**
     * @covers \AHP\Matrix\Calculator::multiplyMatrixes
     */
    public function testMultiply2SizeSquareMatrixes()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';

        $matrixMock1 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 2],
            $criteria2 => [$criteria1 => 3, $criteria2 => 4],
        ]);

        $matrixMock2 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 2, $criteria2 => 0],
            $criteria2 => [$criteria1 => 1, $criteria2 => 2],
        ]);

        $testedInstance = new Calculator(new Arithmetic());
        static::assertEquals(new Matrix([
            $criteria1 => new Row($criteria1, [$criteria1 => 4, $criteria2 => 4]),
            $criteria2 => new Row($criteria2, [$criteria1 => 10, $criteria2 => 8]),
        ]), $testedInstance->multiplyMatrixes($matrixMock1, $matrixMock2));
    }

    /**
     * @covers \AHP\Matrix\Calculator::multiplyMatrixes
     */
    public function testMultiply1RowMatrixTo3RowMatrix()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $dummyIndex = 'dummy';
        $matrixMock1 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 2, $criteria3 => 3],
        ]);
        $matrixMock2 = $this->mocker->prepareMatrix([
            $criteria1 => [$dummyIndex => 4],
            $criteria2 => [$dummyIndex => 5],
            $criteria3 => [$dummyIndex => 6],
        ]);
        $testedInstance = new Calculator(new Arithmetic());
        static::assertEquals(new Matrix([
            $criteria1 => new Row($criteria1, [$dummyIndex => 32])
        ]), $testedInstance->multiplyMatrixes($matrixMock1, $matrixMock2));
    }

    /**
     * @covers \AHP\Matrix\Calculator::multiplyMatrixes
     */
    public function testMultiplyMatrixToEmptyMatrixThrowsException()
    {
        $this->expectExceptionMessage('Matrix Multiplier has no columns.');
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $matrixMock1 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 2, $criteria3 => 3],
        ]);
        $matrixMock2 = $this->mocker->prepareMatrix();
        $testedInstance = new Calculator(new Arithmetic());
        $testedInstance->multiplyMatrixes($matrixMock1, $matrixMock2);
    }

    /**
     * @covers \AHP\Matrix\Calculator::multiplyMatrixes
     */
    public function testMultiply3RowsMatrixTo1RowMatrix()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';

        $matrixMock1 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 4],
            $criteria2 => [$criteria1 => 5],
            $criteria3 => [$criteria1 => 6],
        ]);

        $matrixMock2 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 2, $criteria3 => 3],
        ]);

        $testedInstance = new Calculator(new Arithmetic());
        static::assertEquals(new Matrix([
            $criteria1 => new Row($criteria1, [$criteria1 => 4, $criteria2 => 8, $criteria3 => 12]),
            $criteria2 => new Row($criteria2, [$criteria1 => 5, $criteria2 => 10, $criteria3 => 15]),
            $criteria3 => new Row($criteria3, [$criteria1 => 6, $criteria2 => 12, $criteria3 => 18]),
        ]), $testedInstance->multiplyMatrixes($matrixMock1, $matrixMock2));
    }

    /**
     * @covers \AHP\Matrix\Row::getRowSum
     */
    public function testGetSum()
    {
        $rowMock = $this->createMock(Row::class);
        $rowMock->expects(static::any())->method('getElements')
            ->will(static::returnValue([1, 2, 3]));
        $testedInstance = new Calculator(new Arithmetic());
        static::assertEquals(6, $testedInstance->getRowSum($rowMock));
    }

    /**
     * @covers \AHP\Matrix\Row::getRowProduct
     */
    public function testGetProduct()
    {
        $rowMock = $this->createMock(Row::class);
        $rowMock->expects(static::any())->method('getElements')
            ->will(static::returnValue([1, 2, 3, 4]));
        $testedInstance = new Calculator(new Arithmetic());
        static::assertEquals(24, $testedInstance->getRowProduct($rowMock));
    }

    /**
     * @covers \AHP\Matrix\Row::getRowAverage
     */
    public function testGetAverage()
    {
        $rowMock = $this->createMock(Row::class);
        $rowMock->expects(static::any())->method('getElements')
            ->will(static::returnValue([1, 2, 3, 4]));
        $calculatorArithmetic = new Calculator(new Arithmetic());
        static::assertEquals(2.5, $calculatorArithmetic->getRowAverage($rowMock));
        $calculatorGeometric = new Calculator(new Geometric());
        static::assertEquals(2.2133638394006, $calculatorGeometric->getRowAverage($rowMock));
    }

    /**
     * @covers \AHP\Matrix\Calculator::getRowNVP
     */
    public function testGetRowNVP()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $criteria4 = 'D';

        $row1Mock = $this->createMock(Row::class);
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 3, $criteria4 => 2]));

        $row2Mock = $this->createMock(Row::class);
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 3, $criteria4 => 1]));

        $row3Mock = $this->createMock(Row::class);
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1, $criteria4 => 2]));

        $row4Mock = $this->createMock(Row::class);
        $row4Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 2, $criteria4 => 1]));

        $matrixMock = $this->createMock(Matrix::class);
        $matrixMock->expects(static::any())->method('getRows')
            ->will(static::returnValue([
                    $criteria1 => $row1Mock,
                    $criteria2 => $row2Mock,
                    $criteria3 => $row3Mock,
                    $criteria4 => $row4Mock]
            ));
        $testedInstance = new Calculator(new Geometric());
        static::assertEqualsWithDelta(0.31,
            $testedInstance->getRowNVP($matrixMock, $row1Mock), 0.01);
        static::assertEqualsWithDelta(0.37,
            $testedInstance->getRowNVP($matrixMock, $row2Mock), 0.01);
        static::assertEqualsWithDelta(0.16,
            $testedInstance->getRowNVP($matrixMock, $row3Mock), 0.01);
        static::assertEqualsWithDelta(0.17,
            $testedInstance->getRowNVP($matrixMock, $row4Mock), 0.01);
    }

    /**
     * @covers \AHP\Matrix::getMaxValue
     */
    public function testGetMaxValue()
    {
        $testedInstance = new Calculator(new Geometric());
        $matrixMock = $this->createMock(Matrix::class);

        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $criteria4 = 'D';

        $row1Mock = $this->createMock(Column::class);
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 3, $criteria4 => 2]));

        $row2Mock = $this->createMock(Column::class);
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 3, $criteria4 => 1]));

        $row3Mock = $this->createMock(Column::class);
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1, $criteria4 => 2]));

        $row4Mock = $this->createMock(Column::class);
        $row4Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 2, $criteria4 => 1]));

        // columns
        $column1Mock = $this->createMock(Column::class);
        $column1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 2, $criteria3 => 1 / 3, $criteria4 => 1 / 2]));

        $column2Mock = $this->createMock(Column::class);
        $column2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 3, $criteria4 => 1]));

        $column3Mock = $this->createMock(Column::class);
        $column3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 3, $criteria2 => 3, $criteria3 => 1, $criteria4 => 1 / 2]));

        $column4Mock = $this->createMock(Column::class);
        $column4Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 2, $criteria4 => 1]));

        $matrixMock->expects(static::any())->method('getRow')
            ->will(static::returnValueMap([
                    [$criteria1, $row1Mock],
                    [$criteria2, $row2Mock],
                    [$criteria3, $row3Mock],
                    [$criteria4, $row4Mock]
                ]
            ));
        $matrixMock->expects(static::any())->method('getColumns')
            ->will(static::returnValue([
                    $criteria1 => $column1Mock,
                    $criteria2 => $column2Mock,
                    $criteria3 => $column3Mock,
                    $criteria4 => $column4Mock]
            ));
        $matrixMock->expects(static::any())->method('getRows')
            ->will(static::returnValue([
                    $criteria1 => $row1Mock,
                    $criteria2 => $row2Mock,
                    $criteria3 => $row3Mock,
                    $criteria4 => $row4Mock]
            ));

        static::assertEquals(4.41, $testedInstance->getMatrixMaxValue($matrixMock));
    }

    /**
     * @covers \AHP\Matrix::getConsistencyIndex
     */
    public function testGetConsistencyIndex()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $criteria4 = 'D';

        $row1Mock = $this->createMock(Column::class);
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 3, $criteria4 => 2]));

        $row2Mock = $this->createMock(Column::class);
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 3, $criteria4 => 1]));

        $row3Mock = $this->createMock(Column::class);
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1, $criteria4 => 2]));

        $row4Mock = $this->createMock(Column::class);
        $row4Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 2, $criteria4 => 1]));

        // columns
        $column1Mock = $this->createMock(Column::class);
        $column1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 2, $criteria3 => 1 / 3, $criteria4 => 1 / 2]));

        $column2Mock = $this->createMock(Column::class);
        $column2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 3, $criteria4 => 1]));

        $column3Mock = $this->createMock(Column::class);
        $column3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 3, $criteria2 => 3, $criteria3 => 1, $criteria4 => 1 / 2]));

        $column4Mock = $this->createMock(Column::class);
        $column4Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 2, $criteria4 => 1]));


        $matrixMock = $this->createMock(Matrix::class);
        $matrixMock->expects(static::any())->method('getRow')
            ->will(static::returnValueMap([
                    [$criteria1, $row1Mock],
                    [$criteria2, $row2Mock],
                    [$criteria3, $row3Mock],
                    [$criteria4, $row4Mock]
                ]
            ));
        $matrixMock->expects(static::any())->method('getColumns')
            ->will(static::returnValue([
                    $criteria1 => $column1Mock,
                    $criteria2 => $column2Mock,
                    $criteria3 => $column3Mock,
                    $criteria4 => $column4Mock]
            ));
        $matrixMock->expects(static::any())->method('getRows')
            ->will(static::returnValue([
                    $criteria1 => $row1Mock,
                    $criteria2 => $row2Mock,
                    $criteria3 => $row3Mock,
                    $criteria4 => $row4Mock]
            ));
        $matrixMock->expects(static::any())->method('getSize')
            ->will(static::returnValue(4));

        $testedInstance = new Calculator(new Geometric());
        static::assertEquals(0.14, $testedInstance->getConsistencyIndex($matrixMock));
    }

    /**
     * @covers \AHP\Matrix::getConsistencyRatio
     */
    public function testGetConsistencyRatio()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $criteria4 = 'D';

        $row1Mock = $this->createMock(Column::class);
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 3, $criteria4 => 2]));

        $row2Mock = $this->createMock(Column::class);
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 3, $criteria4 => 1]));

        $row3Mock = $this->createMock(Column::class);
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1, $criteria4 => 2]));

        $row4Mock = $this->createMock(Column::class);
        $row4Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 2, $criteria4 => 1]));

        // columns
        $column1Mock = $this->createMock(Column::class);
        $column1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 2, $criteria3 => 1 / 3, $criteria4 => 1 / 2]));

        $column2Mock = $this->createMock(Column::class);
        $column2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 3, $criteria4 => 1]));

        $column3Mock = $this->createMock(Column::class);
        $column3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 3, $criteria2 => 3, $criteria3 => 1, $criteria4 => 1 / 2]));

        $column4Mock = $this->createMock(Column::class);
        $column4Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 2, $criteria4 => 1]));


        $matrixMock = $this->createMock(Matrix::class);
        $matrixMock->expects(static::any())->method('getRow')
            ->will(static::returnValueMap([
                    [$criteria1, $row1Mock],
                    [$criteria2, $row2Mock],
                    [$criteria3, $row3Mock],
                    [$criteria4, $row4Mock]
                ]
            ));
        $matrixMock->expects(static::any())->method('getColumns')
            ->will(static::returnValue([
                    $criteria1 => $column1Mock,
                    $criteria2 => $column2Mock,
                    $criteria3 => $column3Mock,
                    $criteria4 => $column4Mock]
            ));
        $matrixMock->expects(static::any())->method('getRows')
            ->will(static::returnValue([
                    $criteria1 => $row1Mock,
                    $criteria2 => $row2Mock,
                    $criteria3 => $row3Mock,
                    $criteria4 => $row4Mock]
            ));
        $matrixMock->expects(static::any())->method('getSize')
            ->will(static::returnValue(4));

        $testedInstance = new Calculator(new Geometric());
        static::assertEquals(0.16, $testedInstance->getConsistencyRatio($matrixMock));
    }

    /**
     * Case when task has 3 levels only - like goal, criterias and alternatives
     * @covers \AHP\Matrix\Calculator::getNodeBiggestNVP
     */
    public function testGetNodeBiggestNVPWhenTaskHas3Levels()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $criteria4 = 'D';

        // $nodeMatrix configuration
        $nodeMatrix = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 3, $criteria4 => 2],
            $criteria2 => [$criteria1 => 2, $criteria2 => 1, $criteria3 => 3, $criteria4 => 1],
            $criteria3 => [$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1, $criteria4 => 2],
            $criteria4 => [$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 1 / 2, $criteria4 => 1]
        ]);

        $alternative1 = 'Z';
        $alternative2 = 'S';
        $alternative3 = 'X';

        // $alternativeMatrix1 configuration
        $alternativeMatrix1 = $this->mocker->prepareMatrix([
            $alternative1 => [$alternative1 => 1, $alternative2 => 1 / 2, $alternative3 => 1 / 3],
            $alternative2 => [$alternative1 => 2, $alternative2 => 1, $alternative3 => 1 / 2],
            $alternative3 => [$alternative1 => 3, $alternative2 => 2, $alternative3 => 1],
        ]);

        // $alternativeMatrix2 configuration
        $alternativeMatrix2 = $this->mocker->prepareMatrix([
            $alternative1 => [$alternative1 => 1, $alternative2 => 3, $alternative3 => 5],
            $alternative2 => [$alternative1 => 1 / 3, $alternative2 => 1, $alternative3 => 3],
            $alternative3 => [$alternative1 => 1 / 5, $alternative2 => 1 / 3, $alternative3 => 1],
        ]);

        // $alternativeMatrix3 configuration
        $alternativeMatrix3 = $this->mocker->prepareMatrix([
            $alternative1 => [$alternative1 => 1, $alternative2 => 1 / 2, $alternative3 => 1 / 2],
            $alternative2 => [$alternative1 => 2, $alternative2 => 1, $alternative3 => 1 / 2],
            $alternative3 => [$alternative1 => 2, $alternative2 => 2, $alternative3 => 1],
        ]);

        // $alternativeMatrix4 configuration
        $alternativeMatrix4 = $this->mocker->prepareMatrix([
            $alternative1 => [$alternative1 => 1, $alternative2 => 2, $alternative3 => 3],
            $alternative2 => [$alternative1 => 1 / 2, $alternative2 => 1, $alternative3 => 1 / 2],
            $alternative3 => [$alternative1 => 1 / 3, $alternative2 => 2, $alternative3 => 1],
        ]);

        $subNode1 = $this->mocker->prepareNode($criteria1, $alternativeMatrix1);
        $subNode2 = $this->mocker->prepareNode($criteria2, $alternativeMatrix2);
        $subNode3 = $this->mocker->prepareNode($criteria3, $alternativeMatrix3);
        $subNode4 = $this->mocker->prepareNode($criteria4, $alternativeMatrix4);

        $subNodesTree = [$subNode1, $subNode2, $subNode3, $subNode4];

        $task = $this->mocker->prepareNode('task', $nodeMatrix, $subNodesTree);

        $testedInstance = new Calculator(new Geometric());
        static::assertEquals($alternative1, $testedInstance->getNodeBestAlternative($task));
    }

    /**
     * Case when task has 4 levels - like goal, decision-making specialists, criterias and alternatives
     * @covers \AHP\Processor::getBestOption
     */
    public function testGetBestOptionWhenTaskHas4LevelsExampleMakingDiagnosis()
    {
        $specialist1 = 'S1';
        $specialist2 = 'S2';
        $specialist3 = 'S3';

        $specialistsMatrix = $this->mocker->prepareMatrix([
            $specialist1 => [$specialist1 => 1, $specialist2 => 1, $specialist3 => 5],
            $specialist2 => [$specialist1 => 1, $specialist2 => 1, $specialist3 => 5],
            $specialist3 => [$specialist1 => 1 / 5, $specialist2 => 1 / 5, $specialist3 => 1],
        ]);

        $criteria1 = 'C1';
        $criteria2 = 'C2';
        $criteria3 = 'C3';

        $criteriaMatrixBySpecialist1 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 5],
            $criteria2 => [$criteria1 => 1 / 5, $criteria2 => 1],
        ]);

        $criteriaMatrixBySpecialist2 = $this->mocker->prepareMatrix([
            $criteria2 => [$criteria2 => 1, $criteria3 => 1 / 5],
            $criteria3 => [$criteria2 => 5, $criteria3 => 1],
        ]);

        $criteriaMatrixBySpecialist3 = $this->mocker->prepareMatrix([
            $criteria3 => [$criteria3 => 1]
        ]);

        $decision1 = 'D1';
        $decision2 = 'D2';
        $decision3 = 'D3';
        $decision4 = 'D4';

        $decisionMatrixCriteria1Specialist1 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 3, $decision3 => 1, $decision4 => 1 / 5],
            $decision2 => [$decision1 => 1 / 3, $decision2 => 1, $decision3 => 1 / 3, $decision4 => 1 / 7],
            $decision3 => [$decision1 => 1, $decision2 => 3, $decision3 => 1, $decision4 => 1 / 5],
            $decision4 => [$decision1 => 5, $decision2 => 7, $decision3 => 5, $decision4 => 1]
        ]);

        $decisionMatrixCriteria2Specialist1 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 1 / 5, $decision3 => 1, $decision4 => 1],
            $decision2 => [$decision1 => 5, $decision2 => 1, $decision3 => 5, $decision4 => 5],
            $decision3 => [$decision1 => 1, $decision2 => 1 / 5, $decision3 => 1, $decision4 => 1],
            $decision4 => [$decision1 => 1, $decision2 => 1 / 5, $decision3 => 1, $decision4 => 1],
        ]);

        $decisionMatrixCriteria2Specialist2 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1],
            $decision2 => [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1],
            $decision3 => [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1],
            $decision4 => [$decision1 => 1, $decision2 => 1, $decision3 => 1, $decision4 => 1],
        ]);

        $decisionMatrixCriteria3Specialist2 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 1, $decision3 => 1 / 5, $decision4 => 1],
            $decision2 => [$decision1 => 1, $decision2 => 1, $decision3 => 1 / 5, $decision4 => 1],
            $decision3 => [$decision1 => 5, $decision2 => 5, $decision3 => 1, $decision4 => 5],
            $decision4 => [$decision1 => 1, $decision2 => 1, $decision3 => 1 / 5, $decision4 => 1],
        ]);

        $decisionMatrixCriteria3Specialist3 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 1, $decision3 => 3, $decision4 => 1],
            $decision2 => [$decision1 => 1, $decision2 => 1, $decision3 => 3, $decision4 => 1],
            $decision3 => [$decision1 => 1 / 3, $decision2 => 1 / 3, $decision3 => 1, $decision4 => 1 / 3],
            $decision4 => [$decision1 => 1, $decision2 => 1, $decision3 => 3, $decision4 => 1],
        ]);

        $task = $this->mocker->prepareNode('task', $specialistsMatrix, [
            $this->mocker->prepareNode($specialist1, $criteriaMatrixBySpecialist1, [
                $this->mocker->prepareNode($criteria1, $decisionMatrixCriteria1Specialist1),
                $this->mocker->prepareNode($criteria2, $decisionMatrixCriteria2Specialist1)
            ]),
            $this->mocker->prepareNode($specialist2, $criteriaMatrixBySpecialist2, [
                $this->mocker->prepareNode($criteria2, $decisionMatrixCriteria2Specialist2),
                $this->mocker->prepareNode($criteria3, $decisionMatrixCriteria3Specialist2)
            ]),
            $this->mocker->prepareNode($specialist3, $criteriaMatrixBySpecialist3, [
                $this->mocker->prepareNode($criteria3, $decisionMatrixCriteria3Specialist3)
            ])
        ]);

        $testedInstanceWithGeometricAlgo = new Calculator(new Geometric());
        $testedInstanceWithArithmeticAlgo = new Calculator(new Arithmetic());
        static::assertEquals($decision4, $testedInstanceWithGeometricAlgo->getNodeBestAlternative($task));
        static::assertEquals($decision3, $testedInstanceWithArithmeticAlgo->getNodeBestAlternative($task));
    }

    /**
     * Case when task has 4 levels - like goal, decision-making specialists, criterias and alternatives
     * @covers \AHP\Processor::getBestOption
     */
    public function testGetBestOptionWhenTaskHas4LevelsExampleChoosingHouse()
    {
        $human1 = 'Kevin';
        $human2 = 'June';

        $humansMatrix = $this->mocker->prepareMatrix([
            $human1 => [$human1 => 1, $human2 => 2],
            $human2 => [$human1 => 1/2, $human2 => 1],
        ]);

        $criteria1 = 'Square of ground';
        $criteria2 = 'How far from work';

        $criteriaMatrixByHuman1 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 1/3],
            $criteria2 => [$criteria1 => 3, $criteria2 => 1],
        ]);

        $criteriaMatrixByHuman2 = $this->mocker->prepareMatrix([
            $criteria1 => [$criteria1 => 1, $criteria2 => 4],
            $criteria2 => [$criteria1 => 1/4, $criteria2 => 1],
        ]);

        $decision1 = 'D1';
        $decision2 = 'D2';
        $decision3 = 'D3';

        $decisionMatrixCriteria1Human1 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 2, $decision3 => 3],
            $decision2 => [$decision1 => 1 / 2, $decision2 => 1, $decision3 => 2],
            $decision3 => [$decision1 => 1/3, $decision2 => 1/2, $decision3 => 1],
        ]);

        $decisionMatrixCriteria2Human1 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 2, $decision3 => 1/2],
            $decision2 => [$decision1 => 1 / 2, $decision2 => 1, $decision3 => 1/3],
            $decision3 => [$decision1 => 2, $decision2 => 3, $decision3 => 1],
        ]);

        $decisionMatrixCriteria1Human2 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 4, $decision3 => 2],
            $decision2 => [$decision1 => 1 / 4, $decision2 => 1, $decision3 => 3],
            $decision3 => [$decision1 => 1/2, $decision2 => 1/3, $decision3 => 1],
        ]);

        $decisionMatrixCriteria2Human2 = $this->mocker->prepareMatrix([
            $decision1 => [$decision1 => 1, $decision2 => 1/2, $decision3 => 4],
            $decision2 => [$decision1 => 2, $decision2 => 1, $decision3 => 3],
            $decision3 => [$decision1 => 1/4, $decision2 => 1/3, $decision3 => 1],
        ]);

        $task = $this->mocker->prepareNode('task', $humansMatrix, [
            $this->mocker->prepareNode($human1, $criteriaMatrixByHuman1, [
                $this->mocker->prepareNode($criteria1, $decisionMatrixCriteria1Human1),
                $this->mocker->prepareNode($criteria2, $decisionMatrixCriteria2Human1)
            ]),
            $this->mocker->prepareNode($human2, $criteriaMatrixByHuman2, [
                $this->mocker->prepareNode($criteria1, $decisionMatrixCriteria1Human2),
                $this->mocker->prepareNode($criteria2, $decisionMatrixCriteria2Human2)
            ]),
        ]);

        $testedInstanceWithArithmeticAlgo = new Calculator(new Arithmetic());
        static::assertEquals($decision1, $testedInstanceWithArithmeticAlgo->getNodeBestAlternative($task));
    }
}