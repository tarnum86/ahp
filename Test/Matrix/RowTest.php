<?php

namespace AHP\Test\Matrix;

use AHP\Matrix\Row;
use PHPUnit\Framework\TestCase;

/**
 * Class RowTest
 * @package AHP\Test\Matrix
 */
class RowTest extends TestCase
{
    /**
     * @covers \AHP\Matrix\NumbersRange::getQuantity
     */
    public function testGetQuantity()
    {
        $testedInstance = new Row('test', [1, 2, 3, 4]);
        static::assertEquals(4, $testedInstance->getQuantity());
    }
}