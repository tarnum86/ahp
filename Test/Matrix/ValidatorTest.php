<?php

namespace AHP\Test\Matrix;

use AHP\Matrix\Calculator;
use AHP\Matrix;
use AHP\Matrix\Row;
use PHPUnit\Framework\TestCase;
use AHP\Matrix\ValidatorException;
use AHP\Matrix\Validator;

/**
 * Class ValidatorTest
 * @package AHP\Test\Matrix
 */
class ValidatorTest extends TestCase
{

    /**
     * @covers \AHP\Matrix\Validator::validate
     */
    public function testValidateThrowsExceptionWhenMatrixNotPositive()
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Matrix must have all numbers positive.');

        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';

        $row1Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria1, [$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]));

        $row2Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria2, [$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]));

        $row3Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria3, [$criteria1 => 1, $criteria2 => 1, $criteria3 => -5]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1, $criteria3 => -5]));

        $matrixMock = $this->getMockBuilder(Matrix::class)
            ->setMethodsExcept(['getInverseElement', 'getElement'])->getMock();
        $matrixMock->expects($this->any())->method('getRows')
            ->will($this->returnValue([$criteria1 => $row1Mock, $criteria2 => $row2Mock, $criteria3 => $row3Mock]));
        $matrixMock->expects($this->any())->method('getRow')
            ->will($this->returnValueMap([
                [$criteria1, $row1Mock],
                [$criteria2, $row2Mock],
                [$criteria3, $row3Mock],
            ]));

        $calculator = $this->createMock(Calculator::class);

        $validator = new Validator($calculator);
        $validator->validate($matrixMock);
    }

    /**
     * @covers \AHP\Matrix\Validator::validate
     */
    public function testValidateThrowsExceptionWhenMatrixNotSquare()
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Matrix must be square.');

        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';

        $row1Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria1, [$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]));

        $row2Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria2, [$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1, $criteria3 => 1]));

        $row3Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria3, [$criteria1 => 1, $criteria2 => 1, $criteria3 => 5, 'Excessive element' => 7]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1, $criteria3 => 5, 'Excessive element' => 7]));

        $matrixMock = $this->getMockBuilder(Matrix::class)
            ->setMethodsExcept(['getInverseElement', 'getElement'])->getMock();
        $matrixMock->expects($this->any())->method('getRows')
            ->will($this->returnValue([$criteria1 => $row1Mock, $criteria2 => $row2Mock, $criteria3 => $row3Mock]));
        $matrixMock->expects($this->any())->method('getRow')
            ->will($this->returnValueMap([
                [$criteria1, $row1Mock],
                [$criteria2, $row2Mock],
                [$criteria3, $row3Mock],
            ]));

        $calculatorMock = $this->createMock(Calculator::class);

        $validator = new Validator($calculatorMock);
        $validator->validate($matrixMock);
    }

    /**
     * @covers \AHP\Matrix\Validator::validate
     */
    public function testValidateThrowsExceptionWhenMatrixNotInverse()
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Matrix must be inverse and symmetric.');

        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';

        $row1Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria1, [$criteria1 => 1, $criteria2 => 2, $criteria3 => 3]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 2, $criteria3 => 3]));

        $row2Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria2, [$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 3]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 3]));

        $row3Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria3, [$criteria1 => 1 / 777, $criteria2 => 1 / 3, $criteria3 => 1]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1 / 777, $criteria2 => 1 / 3, $criteria3 => 1]));

        $matrixMock = $this->getMockBuilder(Matrix::class)
            ->setMethodsExcept(['getInverseElement', 'getElement'])->getMock();
        $matrixMock->expects($this->any())->method('getRows')
            ->will($this->returnValue([$criteria1 => $row1Mock, $criteria2 => $row2Mock, $criteria3 => $row3Mock]));
        $matrixMock->expects($this->any())->method('getRow')
            ->will($this->returnValueMap([
                [$criteria1, $row1Mock],
                [$criteria2, $row2Mock],
                [$criteria3, $row3Mock],
            ]));

        $calculatorMock = $this->createMock(Calculator::class);
        $validator = new Validator($calculatorMock);
        $validator->validate($matrixMock);
    }

    /**
     * @covers \AHP\Matrix\Validator::validate
     */
    public function testValidateConsistencyRatioBiggerThanNormal()
    {
        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('Matrix consistency ratio must be less than 0.15.');

        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';

        $row1Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria1, [$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 1 / 3]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row1Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 1 / 3]));

        $row2Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria2, [$criteria1 => 2, $criteria2 => 1, $criteria3 => 1 / 2]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row2Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 2, $criteria2 => 1, $criteria3 => 1 / 2]));

        $row3Mock = $this->getMockBuilder(Row::class)
            ->setConstructorArgs([$criteria3, [$criteria1 => 3, $criteria2 => 2, $criteria3 => 1]])
            ->setMethodsExcept(['getElement'])->getMock();
        $row3Mock->expects($this->any())->method('getElements')
            ->will($this->returnValue([$criteria1 => 3, $criteria2 => 2, $criteria3 => 1]));

        $matrixMock = $this->getMockBuilder(Matrix::class)
            ->setMethodsExcept(['getInverseElement', 'getElement'])->getMock();
        $matrixMock->expects($this->any())->method('getRows')
            ->will($this->returnValue([$criteria1 => $row1Mock, $criteria2 => $row2Mock, $criteria3 => $row3Mock]));
        $matrixMock->expects($this->any())->method('getRow')
            ->will($this->returnValueMap([
                [$criteria1, $row1Mock],
                [$criteria2, $row2Mock],
                [$criteria3, $row3Mock],
            ]));

        $calculatorMock = $this->createMock(Calculator::class);
        $calculatorMock->expects($this->any())->method('getConsistencyRatio')
            ->will($this->returnValue(5));

        $validator = new Validator($calculatorMock);
        $validator->validate($matrixMock);
    }
}