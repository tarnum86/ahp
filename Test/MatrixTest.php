<?php

namespace AHP\Test;

use AHP\Matrix;
use AHP\Matrix\Row;
use AHP\Matrix\Column;
use PHPUnit\Framework\TestCase;

/**
 * Class MatrixTest
 * @package AHP\Test
 */
class MatrixTest extends TestCase
{
    /**
     * @covers \AHP\Matrix::getColumns
     */
    public function testGetColumnsAfterSettingRowsOneByOne()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $testedInstance = new Matrix();
        $testedInstance->addRow(new Row($criteria1, [$criteria1 => 1, $criteria2 => 2, $criteria3 => 3]));
        $testedInstance->addRow(new Row($criteria2, [$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 3]));
        $testedInstance->addRow(new Row($criteria3, [$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1]));

        static::assertEquals([
            $criteria1 => new Column($criteria1, [$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 1 / 3]),
            $criteria2 => new Column($criteria2, [$criteria1 => 2, $criteria2 => 1, $criteria3 => 1 / 3]),
            $criteria3 => new Column($criteria3, [$criteria1 => 3, $criteria2 => 3, $criteria3 => 1])
        ], $testedInstance->getColumns());
    }

    /**
     * @covers \AHP\Matrix::getColumns
     */
    public function testGetColumnsAfterSettingElementsOneByOne()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $testedInstance = new Matrix();
        $testedInstance
            ->setElement($criteria1, $criteria1, 1)
            ->setElement($criteria1, $criteria2, 2)
            ->setElement($criteria1, $criteria3, 3)
            ->setElement($criteria2, $criteria1, 1 / 2)
            ->setElement($criteria2, $criteria2, 1)
            ->setElement($criteria2, $criteria3, 3)
            ->setElement($criteria3, $criteria1, 1 / 3)
            ->setElement($criteria3, $criteria2, 1 / 3)
            ->setElement($criteria3, $criteria3, 1);

        static::assertEquals([
            $criteria1 => new Column($criteria1, [$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 1 / 3]),
            $criteria2 => new Column($criteria2, [$criteria1 => 2, $criteria2 => 1, $criteria3 => 1 / 3]),
            $criteria3 => new Column($criteria3, [$criteria1 => 3, $criteria2 => 3, $criteria3 => 1])
        ], $testedInstance->getColumns());
    }

    /**
     * @covers \AHP\Matrix::getColumns
     */
    public function testGetColumnsAfterSettingAllRowsAtOnce()
    {
        $criteria1 = 'A';
        $criteria2 = 'B';
        $criteria3 = 'C';
        $testedInstance = new Matrix();
        $testedInstance->setRows([
            new Row($criteria1, [$criteria1 => 1, $criteria2 => 2, $criteria3 => 3]),
            new Row($criteria2, [$criteria1 => 1 / 2, $criteria2 => 1, $criteria3 => 3]),
            new Row($criteria3, [$criteria1 => 1 / 3, $criteria2 => 1 / 3, $criteria3 => 1])
        ]);

        static::assertEquals([
            $criteria1 => new Column($criteria1, [$criteria1 => 1, $criteria2 => 1 / 2, $criteria3 => 1 / 3]),
            $criteria2 => new Column($criteria2, [$criteria1 => 2, $criteria2 => 1, $criteria3 => 1 / 3]),
            $criteria3 => new Column($criteria3, [$criteria1 => 3, $criteria2 => 3, $criteria3 => 1])
        ], $testedInstance->getColumns());
    }

}